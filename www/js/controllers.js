angular.module('app.controllers', [])
  
.controller('neopixelWIFICtrl', function($scope,$http) {
	$scope.$on( "$ionicView.enter", function( scopes, states ) {
        if (states.stateId=="menu.neopixelWIFI") {
        	$scope.repaint();
        }
    });
	$scope.repaint=function(){
		
	}
	$scope.openUrl=function(){
		console.log("site")
		window.open('https://github.com/beckmx/Neopixel-Wifi', '_system', 'location=yes');
		
	}
	
})
   
.controller('arduinoSettingsCtrl', function($scope) {
	$scope.arduino=new Object();
	$scope.arduino.IP="127.0.0.1"
	$scope.retrieveStoredIP=function(){
		if(window.localStorage['arduinoIP']!=undefined && window.localStorage['arduinoIP']!=""){
			$scope.arduino.IP=window.localStorage['arduinoIP'];	
		}
	}
	$scope.saveArduinoIP=function(){
		window.localStorage['arduinoIP']=$scope.arduino.IP;	
	}
	$scope.retrieveStoredIP();
})
.controller('colorWheelCtrl', function($scope,$http) {
	$scope.stripe=new Object();
	$scope.stripe.ledNumber=0;
	$scope.stripe.onoff=false;
	$scope.stripe.all=false;
	$scope.stripe.ledAvailable=10;

	window.sendToColor=function(colorrgb){
		console.log("http://"+window.localStorage['arduinoIP']+'/co'+colorrgb)
		$http({
        method: 'GET',
        url: "http://"+window.localStorage['arduinoIP']+'/co'+colorrgb
	      }).then(function successCallback(response) {
	          
	        }, function errorCallback(response) {
	          console.log(response);
	        });
	}

	$scope.updateLedNumber=function(){
		$http({
        method: 'GET',
        url: "http://"+window.localStorage['arduinoIP']+'/nu'+$scope.stripe.ledAvailable
	      }).then(function successCallback(response) {
	          
	        }, function errorCallback(response) {
	          console.log(response);
	        });
	}
	$scope.ledNumberChanged=function(){
		console.log($scope.stripe.ledNumber)
		$http({
        method: 'GET',
        url: "http://"+window.localStorage['arduinoIP']+'/le'+$scope.stripe.ledNumber
	      }).then(function successCallback(response) {
	          
	        }, function errorCallback(response) {
	          console.log(response);
	        });
		}
	$scope.$on( "$ionicView.enter", function( scopes, states ) {
        if (states.stateId=="menu.colorWheel") {
        	window.makeWheel();
        }
    });

    $scope.onOnOff=function(){
    	
    	var stringy="on";
    	if (!$scope.stripe.onoff) {
    		stringy="off"
    	}
    	console.log("http://"+window.localStorage['arduinoIP']+'/'+stringy)
    	$http({
        method: 'GET',
        url: "http://"+window.localStorage['arduinoIP']+'/'+stringy
      }).then(function successCallback(response) {
          
        }, function errorCallback(response) {
          console.log(response);
        });
    }

    $scope.generateRequest=function(){
    	$http({
        method: 'GET',
        url: "http://"+window.localStorage['arduinoIP']+'/fan_projects.json'
      }).then(function successCallback(response) {
          
        }, function errorCallback(response) {
          console.log(response);
        });
    }
	
})
   
.controller('aboutCtrl', function($scope) {

})
    