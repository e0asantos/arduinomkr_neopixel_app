angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('menu.neopixelWIFI', {
    url: '/page1',
    views: {
      'side-menu21': {
        templateUrl: 'templates/neopixelWIFI.html',
        controller: 'neopixelWIFICtrl'
      }
    }
  })

  .state('menu.arduinoSettings', {
    url: '/page2',
    views: {
      'side-menu21': {
        templateUrl: 'templates/arduinoSettings.html',
        controller: 'arduinoSettingsCtrl'
      }
    }
  })

  .state('menu.about', {
    url: '/page3',
    views: {
      'side-menu21': {
        templateUrl: 'templates/about.html',
        controller: 'aboutCtrl'
      }
    }
  })
  .state('menu.colorWheel', {
    url: '/page4',
    views: {
      'side-menu21': {
        templateUrl: 'templates/colorWheel.html',
        controller: 'colorWheelCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    abstract:true
  })

$urlRouterProvider.otherwise('/side-menu21/page1')

  

});