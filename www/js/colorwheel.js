window.makeWheel=function(){
  $(function(){
    $('#colorwheel .colors').each(function(i){
      $(this).attr('id','color_' + ++i);
    });
  });
  
  var speed = 500;
  
  function hex(rgb){
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return '#' +
      ('0' + parseInt(rgb[1],10).toString(16)).slice(-2) +
      ('0' + parseInt(rgb[2],10).toString(16)).slice(-2) +
      ('0' + parseInt(rgb[3],10).toString(16)).slice(-2);
  }
  
  $(function(){
    $('.prev').bind('click',function(){ slide('prev'); });
    $('.next').bind('click',function(){ slide('next'); });
    $('.colors').last().addClass('active');
    function slide($control) {
      var $active = $('.active'),
          $next = $active.next().length ? $active.next() : $('.colors').first(),
          $prev = $active.prev().length ? $active.prev() : $('.colors').last();
      console.log('next',$next);
      console.log('prev',$prev);

      if ($control === 'next'){
        console.log($control);
        $active.removeClass('active');
        $next.addClass('active');
        var $activeBg = $('.active .color').first().css('backgroundColor');
        $('#info .info-bg').animate({ backgroundColor: $activeBg },speed);
        $('#info .arrow').animate({ 'border-bottom-color': $activeBg },speed);
        var big = $('.active .color').first().css('backgroundColor'),
            medium = $('.active .color').first().next().css('backgroundColor'),
            small = $('.active .color').last().css('backgroundColor');
        $('#info .values .big span').text(hex(big));
    	$('#info .values .medium span').text(hex(medium));
    	$('#info .values .small span').text(hex(small));
      }
      if ($control === 'prev'){
        console.log($control);
        $active.removeClass('active');
        $prev.addClass('active');
        var $activeBg = $('.active .color').first().css('backgroundColor');
        $('#info .info-bg').animate({ backgroundColor: $activeBg },speed);
        $('#info .arrow').animate({ 'border-bottom-color': $activeBg },speed);
        var big = $('.active .color').first().css('backgroundColor'),
            medium = $('.active .color').first().next().css('backgroundColor'),
            small = $('.active .color').last().css('backgroundColor');
        $('#info .values .big span').text(hex(big));
    	$('#info .values .medium span').text(hex(medium));
    	$('#info .values .small span').text(hex(small));
      }
      var mrgb=$('.active .color').first().css('backgroundColor').replace(/ /g,'').substring(4);
      mrgb=mrgb.substring(0,mrgb.length-2);
      var mrgbArray=mrgb.split(",")
      for (var i = mrgbArray.length - 1; i >= 0; i--) {
        if (Number(mrgbArray[i])<10) {
          mrgbArray[i]="00"+String(mrgbArray[i])
        } else if (Number(mrgbArray[i])<100) {
          mrgbArray[i]="0"+String(mrgbArray[i])
        }
      }
      
      window.sendToColor(mrgbArray.join(""));

    }
  });
  
  $(function(){
    var $activeBg = $('.active .color').first().css('backgroundColor');
    $('#info .info-bg').css({ backgroundColor: $activeBg });
    $('#info .arrow').css({ 'border-bottom-color': $activeBg });
    var big = $('.active .color').first().css('backgroundColor'),
        medium = $('.active .color').first().next().css('backgroundColor'),
        small = $('.active .color').last().css('backgroundColor');
    $('#info .values .big span').text(hex(big));
    $('#info .values .medium span').text(hex(medium));
    $('#info .values .small span').text(hex(small));
  });
  
  $(function(){
    var $frame = $('.frame'), angle = 0;
    $('.next').rotate({
      bind: {
        click: function(){
          angle -= 30;
          $frame.rotate({ duration: speed*2, animateTo: angle, easing: $.easing.easeOutElastic });
        }
      }
    });
    $('.prev').rotate({
      bind: {
        click: function(){
          angle += 30;
          $frame.rotate({ duration: speed*2, animateTo: angle, easing: $.easing.easeOutElastic });
        }
      }
    });
  });
};
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}
